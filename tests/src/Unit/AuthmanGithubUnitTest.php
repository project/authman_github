<?php

declare(strict_types = 1);

namespace Drupal\Tests\authman_github\Unit;

use Drupal\authman\AuthmanInstance\AuthmanOauthFactory;
use Drupal\authman\Controller\AuthmanOauthAuthorizationCodeController;
use Drupal\authman\Entity\AuthmanAuthInterface;
use Drupal\authman\EntityHandlers\AuthmanAuthStorage;
use Drupal\authman\Plugin\KeyType\OauthClientKeyType;
use Drupal\authman_github\AuthmanInstance\AuthmanOauthGithubInstance;
use Drupal\authman_github\Plugin\AuthmanOauth\AuthmanGithub;
use Drupal\Core\Config\Entity\ConfigEntityStorageInterface;
use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\GeneratedUrl;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\TempStore\PrivateTempStoreFactory;
use Drupal\Core\Url;
use Drupal\Core\Utility\UnroutedUrlAssemblerInterface;
use Drupal\key\KeyInterface;
use Drupal\Tests\UnitTestCase;
use GuzzleHttp\Client;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response as GuzzleResponse;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Drupal\authman\AuthmanInstance\AuthmanOauthInstanceInterface;

/**
 * Authman GitHub.
 *
 * @group authman_github
 */
final class AuthmanGithubUnitTest extends UnitTestCase {

  /**
   * URL generator for testing.
   *
   * @var \Drupal\Core\Routing\UrlGeneratorInterface
   */
  protected $urlGenerator;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    // Create a different container for global.
    $container = new ContainerBuilder();
    $urlAssembler = $this->createMock(UnroutedUrlAssemblerInterface::class);
    $urlAssembler->expects($this->any())
      ->method('assemble')
      ->will($this->returnArgument(0));
    $container->set('unrouted_url_assembler', $urlAssembler);
    $this->urlGenerator = $this->createMock('Drupal\Core\Routing\UrlGeneratorInterface');
    $container->set('url_generator', $this->urlGenerator);
    \Drupal::setContainer($container);
  }

  /**
   * Tests authorization code URL generation.
   */
  public function testAuthorizationCodeUrl() {
    $plugin = $this->createPlugin($this->createMock(ClientInterface::class));

    $providerOptions = [
      'redirectUri' => 'http://example.com/receive/gith',
    ];
    $grantType = 'authorization_code';

    $keyType = $this->createMock(OauthClientKeyType::class);

    $clientKey = $this->createMock(KeyInterface::class);
    $clientKey->expects($this->once())
      ->method('getKeyType')
      ->willReturn($keyType);
    $clientKey->expects($this->once())
      ->method('getKeyValues')
      ->willReturn([
        'client_id' => 'test_client_id',
        'client_secret' => 'test_client_secret',
      ]);

    /** @var \Drupal\authman_github\AuthmanInstance\AuthmanOauthGithubInstance $instance */
    $instance = $plugin->createInstance($providerOptions, $grantType, $clientKey);
    $this->assertInstanceOf(AuthmanOauthGithubInstance::class, $instance);

    // State generation happens in AbstractProvider::getRandomState and happens
    // only after the method is called.
    $url = $instance->authorizationCodeUrl();
    $randomState = $instance->getProvider()->getState();
    $this->assertEquals(32, strlen($randomState));
    $this->assertEquals('https://github.com/login/oauth/authorize?scope=notifications%2Crepo%2Cuser&state=' . $randomState . '&response_type=code&approval_prompt=auto&redirect_uri=http%3A%2F%2Fexample.com%2Freceive%2Fgith&client_id=test_client_id', $url->toString());
  }

  /**
   * Tests authorization token flow.
   *
   * Simulates a user agent hitting the receive route with query args redirected
   * from GitHub. This will capture the request from us to GitHub.
   */
  public function testAuthorizationToken() {
    $mockHandler = new MockHandler();
    $handlerStack = HandlerStack::create($mockHandler);
    $httpClient = new Client(['handler' => $handlerStack]);
    $plugin = $this->createPlugin($httpClient);

    $mockHandler->append(
      new GuzzleResponse(
        200,
        ['Content-Type' => 'application/x-www-form-urlencoded'],
        'access_token=a40characcesstoken&scope=gist%2Cnotifications%2Crepo%2Cuser&token_type=bearer'
      ),
    );

    // Keys.
    $keyType = $this->createMock(OauthClientKeyType::class);
    $clientKey = $this->createMock(KeyInterface::class);
    $clientKey->expects($this->once())
      ->method('getKeyType')
      ->willReturn($keyType);
    $clientKey->expects($this->once())
      ->method('getKeyValues')
      ->willReturn([
        'client_id' => 'test_client_id',
        'client_secret' => 'test_client_secret',
      ]);
    $accessTokenKey = $this->createMock(KeyInterface::class);
    $accessTokenKey->expects($this->once())
      ->method('setKeyValue')
      ->with($this->callback(function ($arg1) {
        // 'expires' is not mockable since it uses time(), so we use this
        // callback to check only a few values.
        return (
          $arg1['access_token'] === 'a40characcesstoken'
          && is_null($arg1['refresh_token'])
          && $arg1['token_type'] === 'bearer')
          && is_null($arg1['expires']);
      }))
      ->willReturnSelf();

    $providerOptions = [
      'redirectUri' => 'http://example.com/receive/gith',
    ];
    $grantType = 'authorization_code';
    /** @var \Drupal\authman_github\AuthmanInstance\AuthmanOauthGithubInstance $instance */
    $instance = $plugin->createInstance($providerOptions, $grantType, $clientKey);

    $messageUrl = $this->createMock(Url::class);
    $messageUrl->method('access')->willReturn(TRUE);
    $accessTokenKey->expects($this->once())
      ->method('toUrl')
      ->withAnyParameters()
      ->willReturn($messageUrl);

    $keyConfigStorage = $this->createMock(ConfigEntityStorageInterface::class);
    $keyConfigStorage->expects($this->exactly(2))
      ->method('load')
      ->with('access_token_key_id')
      ->willReturn($accessTokenKey);
    $entityTypeManager = $this->createMock(EntityTypeManagerInterface::class);
    $entityTypeManager->expects($this->atLeastOnce())
      ->method('getStorage')
      ->with('key')
      ->willReturn($keyConfigStorage);

    $privateStoreFactory = $this->createMock(PrivateTempStoreFactory::class);
    $authmanOauthFactory = $this->createMock(AuthmanOauthFactory::class);
    $authmanOauthFactory->expects($this->once())
      ->method('get')
      ->with('1337')
      ->willReturn($instance);
    $messenger = $this->createMock(MessengerInterface::class);
    $currentUser = $this->createMock(AccountProxyInterface::class);

    $container = new ContainerBuilder();
    $container->set('entity_type.manager', $entityTypeManager);
    $container->set('tempstore.private', $privateStoreFactory);
    $container->set('authman.oauth', $authmanOauthFactory);
    $container->set('messenger', $messenger);
    $container->set('current_user', $currentUser);
    $container->set('string_translation', $this->getStringTranslationStub());
    $codeController = AuthmanOauthAuthorizationCodeController::create($container);

    $request = new Request();
    $request->query->set('code', 'abc123');
    $authmanConfig = $this->createMock(AuthmanAuthInterface::class);
    $authmanConfig->expects($this->any())
      ->method('id')
      ->willReturn('1337');
    $authmanConfig->expects($this->once())
      ->method('getAccessTokenKeyId')
      ->willReturn('access_token_key_id');

    $this->urlGenerator->expects($this->at(0))
      ->method('generateFromRoute')
      ->with('entity.authman_auth.information')
      ->willReturn('/authman/instance/1337');

    /** @var \Symfony\Component\HttpFoundation\RedirectResponse $response */
    $response = $codeController->receive($request, $authmanConfig);
    $this->assertInstanceOf(RedirectResponse::class, $response);
    $this->assertEquals('/authman/instance/1337', $response->getTargetUrl());
  }

  /**
   * Tests authenticated request.
   */
  public function testAuthenticatedRequest() {
    $mockHandler = new MockHandler();
    $handlerStack = HandlerStack::create($mockHandler);
    $httpClient = new Client(['handler' => $handlerStack]);
    $plugin = $this->createPlugin($httpClient);

    $mockHandler->append(
      new GuzzleResponse(
        200,
        ['Content-Type' => 'application/json'], <<<EOT
        [
          {
            "id": 109581435,
            "node_id": "MDEwOlJlcG9zaXRvcnkxMDk1ODE0MzU=",
            "name": "core",
            "full_name": "dpi/core",
            "private": false,
            "owner": {
              "login": "dpi",
              "id": 21850,
              "node_id": "MDQ6VXNlcjIxODUw",
              "avatar_url": "https://avatars2.githubusercontent.com/u/21850?v=4",
              "gravatar_id": "",
              "url": "https://api.github.com/users/dpi",
              "html_url": "https://github.com/dpi",
              "followers_url": "https://api.github.com/users/dpi/followers",
              "following_url": "https://api.github.com/users/dpi/following{/other_user}",
              "gists_url": "https://api.github.com/users/dpi/gists{/gist_id}",
              "starred_url": "https://api.github.com/users/dpi/starred{/owner}{/repo}",
              "subscriptions_url": "https://api.github.com/users/dpi/subscriptions",
              "organizations_url": "https://api.github.com/users/dpi/orgs",
              "repos_url": "https://api.github.com/users/dpi/repos",
              "events_url": "https://api.github.com/users/dpi/events{/privacy}",
              "received_events_url": "https://api.github.com/users/dpi/received_events",
              "type": "User",
              "site_admin": false
            },
            "html_url": "https://github.com/dpi/core",
            "description": "Subtree split of drupal's  /core directory",
            "fork": true,
            "url": "https://api.github.com/repos/dpi/core",
            "forks_url": "https://api.github.com/repos/dpi/core/forks",
            "keys_url": "https://api.github.com/repos/dpi/core/keys{/key_id}",
            "collaborators_url": "https://api.github.com/repos/dpi/core/collaborators{/collaborator}",
            "teams_url": "https://api.github.com/repos/dpi/core/teams",
            "hooks_url": "https://api.github.com/repos/dpi/core/hooks",
            "issue_events_url": "https://api.github.com/repos/dpi/core/issues/events{/number}",
            "events_url": "https://api.github.com/repos/dpi/core/events",
            "assignees_url": "https://api.github.com/repos/dpi/core/assignees{/user}",
            "branches_url": "https://api.github.com/repos/dpi/core/branches{/branch}",
            "tags_url": "https://api.github.com/repos/dpi/core/tags",
            "blobs_url": "https://api.github.com/repos/dpi/core/git/blobs{/sha}",
            "git_tags_url": "https://api.github.com/repos/dpi/core/git/tags{/sha}",
            "git_refs_url": "https://api.github.com/repos/dpi/core/git/refs{/sha}",
            "trees_url": "https://api.github.com/repos/dpi/core/git/trees{/sha}",
            "statuses_url": "https://api.github.com/repos/dpi/core/statuses/{sha}",
            "languages_url": "https://api.github.com/repos/dpi/core/languages",
            "stargazers_url": "https://api.github.com/repos/dpi/core/stargazers",
            "contributors_url": "https://api.github.com/repos/dpi/core/contributors",
            "subscribers_url": "https://api.github.com/repos/dpi/core/subscribers",
            "subscription_url": "https://api.github.com/repos/dpi/core/subscription",
            "commits_url": "https://api.github.com/repos/dpi/core/commits{/sha}",
            "git_commits_url": "https://api.github.com/repos/dpi/core/git/commits{/sha}",
            "comments_url": "https://api.github.com/repos/dpi/core/comments{/number}",
            "issue_comment_url": "https://api.github.com/repos/dpi/core/issues/comments{/number}",
            "contents_url": "https://api.github.com/repos/dpi/core/contents/{+path}",
            "compare_url": "https://api.github.com/repos/dpi/core/compare/{base}...{head}",
            "merges_url": "https://api.github.com/repos/dpi/core/merges",
            "archive_url": "https://api.github.com/repos/dpi/core/{archive_format}{/ref}",
            "downloads_url": "https://api.github.com/repos/dpi/core/downloads",
            "issues_url": "https://api.github.com/repos/dpi/core/issues{/number}",
            "pulls_url": "https://api.github.com/repos/dpi/core/pulls{/number}",
            "milestones_url": "https://api.github.com/repos/dpi/core/milestones{/number}",
            "notifications_url": "https://api.github.com/repos/dpi/core/notifications{?since,all,participating}",
            "labels_url": "https://api.github.com/repos/dpi/core/labels{/name}",
            "releases_url": "https://api.github.com/repos/dpi/core/releases{/id}",
            "deployments_url": "https://api.github.com/repos/dpi/core/deployments",
            "created_at": "2017-11-05T13:23:09Z",
            "updated_at": "2019-11-11T08:32:34Z",
            "pushed_at": "2020-08-18T06:59:07Z",
            "git_url": "git://github.com/dpi/core.git",
            "ssh_url": "git@github.com:dpi/core.git",
            "clone_url": "https://github.com/dpi/core.git",
            "svn_url": "https://github.com/dpi/core",
            "homepage": null,
            "size": 128854,
            "stargazers_count": 0,
            "watchers_count": 0,
            "language": "PHP",
            "has_issues": false,
            "has_projects": true,
            "has_downloads": true,
            "has_wiki": true,
            "has_pages": false,
            "forks_count": 0,
            "mirror_url": null,
            "archived": false,
            "disabled": false,
            "open_issues_count": 8,
            "license": {
              "key": "other",
              "name": "Other",
              "spdx_id": "NOASSERTION",
              "url": null,
              "node_id": "MDc6TGljZW5zZTA="
            },
            "forks": 0,
            "open_issues": 8,
            "watchers": 0,
            "default_branch": "9.0.x",
            "permissions": {
              "admin": true,
              "push": true,
              "pull": true
            }
          }
        ]
        EOT
      ),
    );

    $this->urlGenerator->expects($this->at(0))
      ->method('generateFromRoute')
      ->with('authman.authorization_code.receive')
      ->willReturn((new GeneratedUrl())->setGeneratedUrl('/authman/receive/foo'));

    $keyType = $this->createMock(OauthClientKeyType::class);
    $authmanConfigId = 'foo';

    $authmanConfig = $this->createMock(AuthmanAuthInterface::class);
    $authmanConfig->expects($this->any())
      ->method('id')
      ->willReturn($authmanConfigId);
    $authmanConfig->expects($this->any())
      ->method('getClientKeyId')
      ->willReturn('client_key_id');
    $authmanConfig->expects($this->any())
      ->method('getAccessTokenKeyId')
      ->willReturn('access_token_key_id');
    $authmanConfig->expects($this->any())
      ->method('getPlugin')
      ->willReturn($plugin);
    $authmanConfig->expects($this->any())
      ->method('getGrantType')
      ->willReturn(AuthmanAuthInterface::GRANT_AUTHORIZATION_CODE);

    $clientKey = $this->createMock(KeyInterface::class);
    $clientKey->expects($this->any())
      ->method('getKeyType')
      ->willReturn($keyType);
    $clientKey->expects($this->any())
      ->method('getKeyValues')
      ->willReturn([
        'client_id' => 'test_client_id',
        'client_secret' => 'test_client_secret',
      ]);
    $accessTokenKey = $this->createMock(KeyInterface::class);
    $accessTokenKey->expects($this->any())
      ->method('getKeyType')
      ->willReturn($keyType);
    $accessTokenKey->expects($this->once())
      ->method('getKeyValues')
      ->willReturn([
        'access_token' => 'a179characterrandomtoken',
        'refresh_token' => 'a179characterrandomtoken',
        'token_type' => 'Bearer',
        // Fake expiration.
        'expires' => time() + 3600,
      ]);

    $authmanAuthStorage = $this->createMock(AuthmanAuthStorage::class);
    $authmanAuthStorage->expects($this->any())
      ->method('load')
      ->with('foo')
      ->willReturn($authmanConfig);

    $keyStorage = $this->createMock(ConfigEntityStorageInterface::class);
    $keyStorage->expects($this->at(0))
      ->method('load')
      ->with('client_key_id')
      ->willReturn($clientKey);
    $keyStorage->expects($this->at(1))
      ->method('load')
      ->with('access_token_key_id')
      ->willReturn($accessTokenKey);
    $entityTypeManager = $this->createMock(EntityTypeManagerInterface::class);
    $entityTypeManager->expects($this->any())
      ->method('getStorage')
      ->willReturnMap([
        ['key', $keyStorage],
        ['authman_auth', $authmanAuthStorage],
      ]);

    $authmanOauthFactory = new AuthmanOauthFactory($entityTypeManager);
    $authmanInstance = $authmanOauthFactory->get($authmanConfigId);
    $this->assertInstanceOf(AuthmanOauthInstanceInterface::class, $authmanInstance);

    $response = $authmanInstance->authenticatedRequest('GET', 'https://api.github.com/user/repos');
    $this->assertEquals('application/json', $response->getHeader('Content-Type')[0]);
    $json = \json_decode((string) $response->getBody());
    $this->assertCount(1, $json);
    $this->assertEquals('dpi/core', $json[0]->full_name);
  }

  /**
   * Creates a plugin for testing.
   *
   * @param \GuzzleHttp\ClientInterface $httpClient
   *   The HTTP client.
   *
   * @return \Drupal\authman_github\Plugin\AuthmanOauth\AuthmanGithub
   *   The plugin for testing.
   */
  protected function createPlugin(ClientInterface $httpClient): AuthmanGithub {
    $container = $this->createMock(ContainerInterface::class);
    $container->expects($this->once())
      ->method('get')
      ->with('http_client')
      ->willReturn($httpClient);

    $plugin = AuthmanGithub::create($container, [], '', []);
    $plugin->setConfiguration([
      'offline' => TRUE,
      'scopes' => [
        'notifications',
        'repo',
        'user',
      ],
    ]);

    return $plugin;
  }

}
