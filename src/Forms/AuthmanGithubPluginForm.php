<?php

declare(strict_types = 1);

namespace Drupal\authman_github\Forms;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\PluginFormBase;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Form for configuring GitHub plugins.
 *
 * @property \Drupal\authman_github\Plugin\AuthmanOauth\AuthmanGithub $plugin
 */
class AuthmanGithubPluginForm extends PluginFormBase {

  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state): array {
    $configuration = $this->plugin->getConfiguration();

    $options = [];

    // Scopes found at scopes are at https://docs.github.com/en/free-pro-team@latest/developers/apps/scopes-for-oauth-apps#available-scopes.
    $options['repo'] = [
      $this->t('repo'),
      $this->t('Grants full access to private and public repositories. That includes read/write access to code, commit statuses, repository and organization projects, invitations, collaborators, adding team memberships, deployment statuses, and repository webhooks for public and private repositories and organizations. Also grants ability to manage user projects.'),
    ];
    $options['repo:status'] = [
      $this->t('- repo:status'),
      $this->t('Grants read/write access to public and private repository commit statuses. This scope is only necessary to grant other users or services access to private repository commit statuses without granting access to the code.'),
    ];
    $options['repo_deployment'] = [
      $this->t('repo_deployment'),
      $this->t('Grants access to deployment statuses for public and private repositories. This scope is only necessary to grant other users or services access to deployment statuses, without granting access to the code.'),
    ];
    $options['public_repo'] = [
      $this->t('- public_repo'),
      $this->t('Limits access to public repositories. That includes read/write access to code, commit statuses, repository projects, collaborators, and deployment statuses for public repositories and organizations. Also required for starring public repositories.'),
    ];
    $options['repo:invite'] = [
      $this->t('- repo:invite'),
      $this->t('Grants accept/decline abilities for invitations to collaborate on a repository. This scope is only necessary to grant other users or services access to invites without granting access to the code.'),
    ];
    $options['security_events'] = [
      $this->t('security_events'),
      $this->t('Grants read and write access to security events in the code scanning API.'),
    ];
    $options['admin:repo_hook'] = [
      $this->t('- admin:repo_hook'),
      $this->t('Grants read, write, ping, and delete access to repository hooks in public and private repositories. The repo and public_repo scopes grants full access to repositories, including repository hooks. Use the admin:repo_hook scope to limit access to only repository hooks.'),
    ];
    $options['write:repo_hook'] = [
      $this->t('write:repo_hook'),
      $this->t('Grants read, write, and ping access to hooks in public or private repositories.'),
    ];
    $options['read:repo_hook'] = [
      $this->t('read:repo_hook'),
      $this->t('Grants read and ping access to hooks in public or private repositories.'),
    ];
    $options['admin:org'] = [
      $this->t('admin:org'),
      $this->t('Fully manage the organization and its teams, projects, and memberships.'),
    ];
    $options['write:org'] = [
      $this->t('- write:org'),
      $this->t('Read and write access to organization membership, organization projects, and team membership.'),
    ];
    $options['read:org'] = [
      $this->t('- read:org'),
      $this->t('Read-only access to organization membership, organization projects, and team membership.'),
    ];
    $options['admin:public_key'] = [
      $this->t('admin:public_key'),
      $this->t('Fully manage public keys.'),
    ];
    $options['write:public_key'] = [
      $this->t('write:public_key'),
      $this->t('Create, list, and view details for public keys.'),
    ];
    $options['read:public_key'] = [
      $this->t('read:public_key'),
      $this->t('List and view details for public keys.'),
    ];
    $options['admin:org_hook'] = [
      $this->t('admin:org_hook'),
      $this->t('Grants read, write, ping, and delete access to organization hooks. Note: OAuth tokens will only be able to perform these actions on organization hooks which were created by the OAuth App. Personal access tokens will only be able to perform these actions on organization hooks created by a user.'),
    ];
    $options['gist'] = [
      $this->t('gist'),
      $this->t('Grants write access to gists.'),
    ];
    $options['notifications'] = [
      $this->t('notifications'),
      $this->t("Grants: read access to a user's notifications, mark as read access to threads, mark as read access to threads, watch and unwatch access to a repository, and read, write, and delete access to thread subscriptions."),
    ];
    $options['user'] = [
      $this->t('user'),
      $this->t('Grants read/write access to profile info only. Note that this scope includes user:email and user:follow.'),
    ];
    $options['read:user'] = [
      $this->t('- read:user'),
      $this->t("Grants access to read a user's profile data."),
    ];
    $options['user:email'] = [
      $this->t('- user:email'),
      $this->t("Grants read access to a user's email addresses."),
    ];
    $options['user:follow'] = [
      $this->t('- user:follow'),
      $this->t('Grants access to follow or unfollow other users.'),
    ];
    $options['delete_repo'] = [
      $this->t('delete_repo'),
      $this->t('Grants access to delete adminable repositories.'),
    ];
    $options['write:discussion'] = [
      $this->t('write:discussion'),
      $this->t('Allows read and write access for team discussions.'),
    ];
    $options['read:discussion'] = [
      $this->t('read:discussion'),
      $this->t('Allows read access for team discussions.'),
    ];
    $options['write:packages'] = [
      $this->t('write:packages'),
      $this->t('Grants access to upload or publish a package in GitHub Packages. For more information, see "Publishing a package".'),
    ];
    $options['read:packages'] = [
      $this->t('read:packages'),
      $this->t('Grants access to download or install packages from GitHub Packages. For more information, see "Installing a package".'),
    ];
    $options['delete:packages'] = [
      $this->t('delete:packages'),
      $this->t('Grants access to delete packages from GitHub Packages. For more information, see "Deleting packages".'),
    ];
    $options['admin:gpg_key'] = [
      $this->t('admin:gpg_key'),
      $this->t('Fully manage GPG keys.'),
    ];
    $options['write:gpg_key'] = [
      $this->t('- write:gpg_key'),
      $this->t('Create, list, and view details for GPG keys.'),
    ];
    $options['read:gpg_key'] = [
      $this->t('- read:gpg_key'),
      $this->t('List and view details for GPG keys.'),
    ];
    $options['workflow'] = [
      $this->t('workflow'),
      $this->t('Grants the ability to add and update GitHub Actions workflow files. Workflow files can be committed without this scope if the same file (with both the same path and contents) exists on another branch in the same repository. Workflow files can expose GITHUB_TOKEN which may have a different set of scopes, see https://docs.github.com/en/free-pro-team@latest/actions/reference/authentication-in-a-workflow#permissions-for-the-github_token for details.'),
    ];

    $form['scopes'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Scopes'),
      '#description' => $this->t('If no scopes are selected: Grants read-only access to public information (includes public user profile info, public repository info, and gists)'),
      '#options' => array_combine(array_keys($options), array_column($options, 0)),
      '#default_value' => $configuration['scopes'],
    ];
    foreach ($options as $key => [1 => $description]) {
      $form['scopes'][$key]['#description'] = $description;
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state): void {
    $config = [
      'scopes' => array_keys(array_filter($form_state->getValue('scopes', []))),
    ];
    $this->plugin->setConfiguration($config);
  }

}
